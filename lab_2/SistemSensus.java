import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author irfanamrl, NPM 1706039585, Kelas D, GitLab Account: .....
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		
		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = 0;
		try {
			panjang = input.nextInt();
		} catch (Exception e) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");System.exit(0);
		}
		
		
		if (panjang<0 || panjang > 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		
		int lebar = input.nextInt();
		if (lebar < 0 || panjang > 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");System.exit(0);
		}
		
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = input.nextInt();
		if ( tinggi < 0 || tinggi > 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		double berat = input.nextDouble();
		if ( berat < 0 || berat > 150){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		int makanan = input.nextInt();
		if ( makanan < 0 || makanan > 20){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.next();
		System.out.print("Catatan Tambahan       : ");
		input.nextLine();
		String catatan = input.nextLine().trim();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = input.nextInt();
		input.nextLine();
		


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double floatpanjang = (double) panjang;
		double floatlebar = (double) lebar;
		double floattinggi = (double) tinggi;
		double rasiobaru = ((berat)/ ((floatpanjang * 0.01) * (floatlebar * 0.01) * (floattinggi*0.01)));
		int ratio = (int) rasiobaru;
		for (int i =0; i<jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + (i+1) + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase
			String besar = penerima.toUpperCase();
			// TODO Periksa ada catatan atau tidak
			
			if (catatan.isEmpty()) catatan = "Tidak ada catatan";

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			System.out.println("DATA SIAP DICETAK UNTUK " + besar + "\n"+
				"--------------------\n" +
				nama +"-"+ alamat +"\n"+ "Lahir pada tanggal "+
				tanggalLahir+"\n"+ "Rasio Berat Per Volume   = "+ ratio+
				"kg/m^3"+ "\n"+ "Catatan: "+ catatan+ "\n");
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int total = 0;
		for (int i=0; i<nama.length(); i++){
			total += (int)nama.charAt(i);
		}
		int hitung = (((panjang*tinggi*lebar)+ total)%10000);


		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(1) + Integer.toString(hitung);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (50000)*(365)*(makanan);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String tahunLahir = tanggalLahir.substring(6,10); // lihat hint jika bingung
		int umur = (2018)-(Integer.parseInt(tahunLahir));

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String Apartemen = "";
		String Kabupaten = "";
		
		if (anggaran <= 100000000 && umur > 18 && umur < 1019){
			Apartemen = "PPMT";
			Kabupaten = "Sastra";
		}
		else if(anggaran > 100000000 && umur > 18 && umur < 1019){
			Apartemen = "Mares";
			Kabupaten = "Margonda";
		}
		else if(umur <=18){
			Apartemen = "PPMT";
			Kabupaten = "Rotunda";
		}
		else {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "REKOMENDASI APARTEMEN\n--------------------";
		System.out.println(rekomendasi);
		System.out.println("MENGETAHUI: Identitas keluarga: "+ nama +"-"+ nomorKeluarga+
		"\nMENIMBANG: Anggaran Makanan Tahunan: Rp "+ anggaran +
		"\n           Umur Kepala Keluarga: "+ umur + "tahun"+ "\nMEMUTUSKAN: keluarga "+
		nama + ", akan ditempatkan di:\n"+ Apartemen + ", kabupaten " + Kabupaten);

		input.close();
		Mobil a = new Mobil();
		a.jalan();
		
		Mobil.jalan();
		Mobil.roda;
	}
}

class Mobil {
	public static int roda = 4;
	public static void jalan() {
	public static int banyakMobilDiproduksi = 0;
}