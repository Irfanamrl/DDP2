package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.math.BigInteger;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        if (getEvent(name) != null) {
            return "Event "+name+" sudah ada!";
        }
        else {
            String[] parts = startTimeStr.split("_");
            String[] tanggalMulai = parts[0].split("-");
            String[] jamMulai = parts[1].split(":");
            GregorianCalendar start = new GregorianCalendar(Integer.parseInt(tanggalMulai[0]), Integer.parseInt(tanggalMulai[1]), Integer.parseInt(tanggalMulai[2]),
                    Integer.parseInt(jamMulai[0]), Integer.parseInt(jamMulai[1]), Integer.parseInt(jamMulai[2]));
            String[] parts2 = endTimeStr.split("_");
            String[] tanggalAkhir = parts2[0].split("-");
            String[] jamAkhir = parts2[1].split(":");
            GregorianCalendar end = new GregorianCalendar(Integer.parseInt(tanggalAkhir[0]), Integer.parseInt(tanggalAkhir[1]), Integer.parseInt(tanggalAkhir[2]),
                    Integer.parseInt(jamAkhir[0]), Integer.parseInt(jamAkhir[1]), Integer.parseInt(jamAkhir[2]));
            BigInteger biaya = new BigInteger(costPerHourStr);
            if (start.compareTo(end) < 0) {
                events.add(new Event(name, start, end, biaya));
                return "Event " + name + " berhasil ditambahkan!";
            }
            else {
                return "Waktu yang diinputkan tidak valid";
            }

        }
    }
    
    public String addUser(String name)
    {
        // TODO: Implement:
        if (users.contains(name)) {
            return "User "+ name +" sudah ada!";
        }
        else{
            users.add(new User(name));
            return "User "+ name + " berhasil ditambahkan!";
        }
    }
    
    public String registerToEvent(String userName, String eventName)
    {
        User user = getUser(userName);
        Event event = getEvent(eventName);
        boolean isRegistrable = true;

        if (user != null || event != null) {
            if (user != null) {
                if (event != null) {
                    for (Event e : user.getEvents()) {
                        isRegistrable &= event.isCompatible(e);
                    }
                    if (isRegistrable) {
                        user.addEvent(event);
                        return userName + " berencana menghadiri " + eventName + "!";
                    }
                    return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
                }
                return " Tidak ada acara dengan nama " + eventName + "!";
            }
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }
        return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
    }


    public User getUser(String name) {
        for (User u : users) {
            if (u.getName().equals(name)) {
                return u;
            }
        }
        return null;
    }

    public Event getEvent(String name) {
        for (Event e : events) {
            if (e.getName().equals(name)) {
                return e;
            }
        }
        return null;
    }
}