package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    private GregorianCalendar waktuMulai;
    private GregorianCalendar waktuAkhir;
    private BigInteger costPerHour;

    public Event (String name, GregorianCalendar waktuMulai, GregorianCalendar waktuAkhir, BigInteger costPerHour) {
        this.name = name;
        this.waktuMulai = waktuMulai;
        this.waktuAkhir = waktuAkhir;
        this.costPerHour = costPerHour;
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */ 
    public String getName()
    {
        return this.name;
    }
    
    // TODO: Implement toString()
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.

    public BigInteger getCost() {
        return costPerHour;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return this.name +
                "\nWaktu mulai: " + sdf.format(this.waktuMulai.getTime()) +
                "\nWaktu selesai: " + sdf.format(this.waktuAkhir.getTime()) +
                "\nBiaya kehadiran: " + costPerHour.toString();
    }

    public boolean isCompatible(Event other) {
        if (waktuMulai.before(other.waktuMulai) && waktuAkhir.before(other.waktuMulai)) {
            return true;
        }
        else if (other.waktuMulai.before(waktuMulai) && other.waktuAkhir.before(waktuMulai)) {
            return true;
        }
        else if (waktuAkhir.compareTo(other.waktuMulai) == 0 || other.waktuAkhir.compareTo(waktuMulai) == 0) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Event event) {
        return waktuMulai.compareTo(event.waktuMulai);
    }

    public Event cloneEvent() {
        Event temp = new Event(this.name, this.waktuMulai, this.waktuAkhir, this.costPerHour);
        return temp;
    }
}
