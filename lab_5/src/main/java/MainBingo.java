import java.util.Scanner;

public class MainBingo{
    public static void main(String[] args){
        Number angka;
        int nilaiangka;
        Number[][] numbers = new Number[5][5];
        Scanner input = new Scanner(System.in);
        for (int a = 0; a<5 ; a++){
            for (int b = 0; b < 5; b++){
                nilaiangka = input.nextInt();
                angka = new Number(nilaiangka, a, b);
                numbers[a][b] = angka;
            }
        }
        Number[] states = new Number[100];
        for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){				
				states[numbers[i][j].getValue()] = numbers[i][j];
			}
        }
        input.nextLine();
        BingoCard bingoboard = new BingoCard(numbers, states);
        while(true){
            String perintah = input.nextLine();
            String[] splitted = perintah.split(" ");
            if (splitted[0].equals("MARK")){
                System.out.println(bingoboard.markNum(Integer.parseInt(splitted[1])));
            }
            else if (splitted[0].equals("RESTART")){
                bingoboard.restart();
            }
            else if (splitted[0].equals("INFO")){
                System.out.println(bingoboard.info());
            }
            else{
                System.out.println("perintah salah");
            }
        }
    }
}