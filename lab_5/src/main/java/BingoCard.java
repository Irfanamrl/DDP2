/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		String output;
		if (numberStates[num] == null){
			output = "Kartu tidak memiliki angka " + num;
		}
		else {
			if (this.numberStates[num].isChecked()){
				output = num+" sebelumnya sudah tersilang";
			}
			else{
				numberStates[num].setChecked(true);
				output = num+" tersilang";
			}
		}
		cekBingo();
		return output;
	}
	
	public String info(){
		String output = "";
		for (int i = 0; i < 5; i++){
			for (int j = 0; j < 5; j++){
				if (numbers[i][j].isChecked()){
					output += ("| X  ");
				}
				else{
					output += ("| "+(numbers[i][j].getValue())+" ");
				}	
			}
			if(i==4){
				output += "|";
			}else{
				output += "|\n";
			}
		}
		return output;
	}

	public void cekBingo(){
		//cek vertically
		for (int i  = 0; i<5 ; i++){
			if (numbers[i][0].isChecked() && 
				numbers[i][1].isChecked() && 
				numbers[i][2].isChecked() && 
				numbers[i][3].isChecked() && 
				numbers[i][4].isChecked()){
				System.out.println("BINGO!");
				this.setBingo(true);
				info();
			}
			else if (numbers[0][i].isChecked() && 
				numbers[1][i].isChecked() && 
				numbers[2][i].isChecked() && 
				numbers[3][i].isChecked() && 
				numbers[4][i].isChecked()){
				System.out.println("BINGO!");
				this.setBingo(true);
				info();
			}
		}
		if (numbers[0][0].isChecked() && 
			numbers[1][1].isChecked() && 
			numbers[2][2].isChecked() && 
			numbers[3][3].isChecked() && 
			numbers[4][4].isChecked()){
			System.out.println("BINGO!");
			this.setBingo(true);
			info();
		}
		else if (numbers[4][0].isChecked() && 
		numbers[3][1].isChecked() && 
		numbers[2][2].isChecked() && 
		numbers[1][3].isChecked() && 
		numbers[0][4].isChecked()){
			System.out.println("BINGO!");
			this.setBingo(true);
			info();
		}
	}
	
	public void restart(){
		//TODO Implement
		for (Number[] nums : numbers){
			for (Number num: nums) {
				num.setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
	

}
