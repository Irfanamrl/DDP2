import java.util.Scanner;

public class RabbitHouse {
    public static int jumlah(int panjang) {
        if (panjang == 1) {
            return 1;
        }
        else{
            return 1 + (jumlah(panjang-1)*panjang);
        }
    }
    public static boolean palindrome(String s){
        int length = s.length();
        if (length < 2){
            return true;
        }
        else{
            if (s.charAt(0) != s.charAt(length-1)){
                return false;
            }
            else{
                return palindrome(s.substring(1, length-1));
            }
        }
    }
    public static int counting(String a){
        int pengitung = 0;
        if (!palindrome(a)){
            pengitung++;
            for (int i = 0; i < a.length(); i++){
                pengitung += counting(a.substring(0,i) + a.substring(i+1));
            }
        }
        return pengitung;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan nama kelinci: ");
        String Nama = sc.nextLine();
        String[] kata = Nama.split("\\s");
        String Metode = kata[0];
        String namaKelinci = kata[1];
        int panjang = kata[1].length();
        if (Metode.equals("normal")){
            System.out.println(jumlah(panjang));
        }
        else if (Metode.equals("palindrom")){
            System.out.println(counting(namaKelinci));
        }
        else{
            System.out.println("Error");
        }
        sc.close();
    }
}