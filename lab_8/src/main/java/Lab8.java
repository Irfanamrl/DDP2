import java.util.ArrayList;
import java.util.Scanner;

public class Lab8 {
    public static ArrayList<Karyawan> pegawai = new ArrayList<Karyawan>();
    public static int maksGaji;
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int gaji = in.nextInt();
        maksGaji = gaji;

        do {
            String perintah = in.nextLine();
            String[] masukan = perintah.split(" ");
            if (masukan[0].equals("TAMBAH_KARYAWAN")) {
                if (find(masukan[2]) == null) {
                    int foo = Integer.parseInt(masukan[3]);
                    Karyawan dummy = tambahKaryawan(masukan[1], masukan[2], foo);
                    pegawai.add(dummy);
                    System.out.println(masukan[2] + " mulai bekerja sebagai "+ masukan[1] +" di PT. NAMPAN");
                }
                else {
                    System.out.println("Karyawan tidak ditemukan");
                }
            }
            else if (masukan[0].equals("STATUS")) {
                if (find(masukan[1]) == null) {
                    System.out.println("Karyawan tidak ditemukan");
                }
                else {
                    Karyawan i = find(masukan[1]);
                    System.out.println(masukan[1] + " " + i.getGaji());
                }
            }
            else if (masukan[0].equals("TAMBAH_BAWAHAN")) {
                if (find(masukan[1])== null | find(masukan[2])== null) {
                    System.out.println("Nama tidak berhasil ditemukan");
                }
                else {
                    Karyawan i = find(masukan[1]);
                    Karyawan j = find(masukan[2]);
                    if (i.getTipe().equals("Manager") | i.getTipe().equals("Staff")) {
                        if (i.getBawahan().contains(j) != true) {
                            i.addBawahan(j);
                            System.out.printf("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n", masukan[2], masukan[1]);    
                        }
                        else {
                            System.out.printf("Karyawan %s telah menjadi bawahan %s\n", masukan[2], masukan[1]);
                        }
                    }
                    else {
                        System.out.println("Anda tidak layak memiliki bawahan");
                    }
                }
            }
            else if (masukan[0].equals("GAJIAN")) {
                System.out.println("Semua karyawan telah diberikan gaji");
                for (Karyawan i: pegawai) {
                    i.gajian();
                    if (i.getGaji() > maksGaji) {
                        if (i.getTipe().equals("Staff")) {
                            String tempNama = i.getNama();
                            int tempGaji = i.getGaji();
                            pegawai.remove(i);
                            ArrayList<Karyawan> copy = i.getBawahan();
                            Karyawan baru = new Manager(tempNama, tempGaji);
                            pegawai.add(baru);
                            baru.bawahan = copy;
                            System.out.printf("Selamat, %s telah dipromosikan menjadi MANAGER\n", tempNama);
                            for (Karyawan atasan : pegawai) {
                                if (atasan.getBawahan().contains(i)) {
                                    atasan.getBawahan().remove(i);
                                }
                            }
                        }
                    }
                }
            }
            else if (masukan[0].equals("PRINT_BAWAHAN")) {
                if (find(masukan[1])== null) {
                    System.out.println("Nama tidak berhasil ditemukan");
                }
                else {
                    Karyawan i = find(masukan[1]);
                    for (Karyawan j: i.getBawahan()) {
                        System.out.println(j.getNama() + " " + j.getTipe().toUpperCase());
                    }
                }
            }
        } while(pegawai.size() < 10001);
    }

    private static Karyawan tambahKaryawan(String tipe, String nama, int gaji){
        if (tipe.equals("MANAGER")) {
            return new Manager(nama, gaji);
        }

        else if (tipe.equals("STAFF")) {
            return new Staff(nama, gaji);
        }

        else if (tipe.equals("INTERN")) {
            return new Intern(nama, gaji);
        }
        return null;
    }

    private static Karyawan find(String nama) {
        for (Karyawan i : pegawai) {
            if (i.getNama().equals(nama)) {
                return i;
            }
        }
        return null;
    }
}