import java.util.ArrayList;

public abstract class Karyawan {
    protected String nama;
    protected String jabatan;
    protected int gaji;
    protected ArrayList<Karyawan> bawahan = new ArrayList<Karyawan>();
    protected int ctr;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTipe() {
        return jabatan;
    }

    public void setTipe(String jabatan) {
        this.jabatan = jabatan;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public ArrayList<Karyawan> getBawahan() {
        return bawahan;
    }

    public void addBawahan(Karyawan nama) {
        bawahan.add(nama);
    }

    public void gajian() {
        ctr += 1;
        if (ctr % 6 == 0) {
            naikGaji();
        }
    }

    public void naikGaji() {
        int temp = (gaji * 110)/100;
        System.out.printf("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d\n", nama, gaji, temp);
        gaji = temp;
    }
}