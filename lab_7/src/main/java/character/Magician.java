package character;

public class Magician extends Player {

    public Magician(String nama, int darah) {
        super(nama, darah);
        this.tipe = "Magician";
    }

    public String burn(Player dua) {
        if (dua.getMati() == false) {
            if (dua.getTipe().equals("Magician")) {
                if (dua.getHp() <= 20) {
                    dua.setDarah(-20);
                    dua.setTerbakar();
                    return "Nyawa " + dua + " " + dua.getHp() + "\n dan Matang";
                }
                else {
                    dua.setDarah(-20);
                    return "Nyawa " + dua + " " + dua.getHp();
                }
            }
            else {
                dua.setDarah(-10);
                return "Nyawa " + dua + " " + dua.getHp();
            }
        }
        else {
            dua.setTerbakar();
            return "Nyawa " + dua + " " + dua.getHp() + "\n dan Matang";
        }
    }
}
//  write Magician Class here