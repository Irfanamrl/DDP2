package character;
import java.util.ArrayList;

public class Player {
    protected int darah;
    protected String nama;
    protected boolean terbakar;
    protected boolean mati;
    protected String tipe;
    protected ArrayList<Player> termakan = new ArrayList<Player>();


    public Player(String nama, int darah) {
        this.nama = nama;
        this.darah = darah;
        this.mati = false;
        if (darah > 0) {
            this.darah = darah;
        }
        else {
            this.darah = 0;
            this.setMati();
        }
        this.terbakar = false;
        this.tipe = tipe;
        this.termakan = new ArrayList<>();
    }

    public String getName() {
        return this.nama;
    }

    public int getHp() {
        return this.darah;
    }

    public String getTipe() {
        return this.tipe;
    }

    public boolean getMati() {
        return this.mati;
    }

    public boolean getTerbakar() {
        return this.terbakar;
    }

    public ArrayList<Player> getTermakan() {
        return this.termakan;
    }

    public void setTerbakar() {
        this.terbakar = true;
    }

    public void setMati() {
        this.mati = true;
    }
    public void setDarah(int darah) {
        if (this.darah + darah <= 0) {
            this.darah = 0;
            this.setMati();
        }
        else {
            this.darah = this.darah + darah;
        }
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void addTermakan(Player nama) {
        termakan.add(nama);
    }
    public String attack(Player korban) {
        if (korban.getTipe().equals("Magician")) {
            korban.setDarah(-20);
        }
        else {
            korban.setDarah(-10);
        }
        return "Nyawa " + korban.getName() + " " + korban.getHp();
    }

    public boolean canEat(Player eaten) {
        if (this.tipe.equals("Human") || this.tipe.equals("Magician")) {
            if (eaten.getTipe().equals("Human")) {
                return false;
            }
            else if(eaten.getTipe().equals("Magician")) {
                return false;
            }
            else if (eaten.getTipe().equals("Monster")) {
                if (eaten.getMati() == false) {
                    return false;
                }
                else if (eaten.getTerbakar() == false) {
                    return false;
                }
                else {
                    return true;
                }
            }
        } 
        else if (this.tipe.equals("Monster")) {
            if (eaten.getMati() == true) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }
}
//  write Player Class here
