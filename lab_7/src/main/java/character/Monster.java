    package character;

public class Monster extends Player {
    String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String nama, int darah) {
        super(nama, darah*2);
        this.tipe = "Monster";
    }

    public Monster(String nama, int darah, String roar) {
        super(nama, darah*2);
        this.tipe = "Monster";
        this.roar = roar;
    }
    
    public String roar() {
        return this.roar;
    }
}
//  write Monster Class here