import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> dataDimakan = new ArrayList<Player>();
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player i : player) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        Player dummy;
        if (find(chara) == null) {
            if(tipe.equals("Human")) {
                dummy = new Human(chara, hp);
                player.add(dummy);
            }
            else if(tipe.equals("Magician")) {
                dummy = new Magician(chara, hp);
                player.add(dummy);
            }
            else if (tipe.equals("Monster")) {
                dummy = new Monster(chara, hp);
                player.add(dummy);
            }
            return chara + " ditambah ke game";
        }
        else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        Player dummy;
        if (tipe.equals("Monster")) {
            if (find(chara)==null) {
                dummy = new Monster(chara, hp, roar);
                return chara + " ditambah ke game";
            }
            else {
                return "Sudah ada karakter bernama " + chara;
            }
        }
        else {
            return "bukan monster";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if (find(chara) != null) {
            Player i = find(chara);
            player.remove(i);
            return chara + " dihapus dari game";
        }
        else {
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        String status = "";
        if (find(chara) == null) {
            return "Tidak ada " + chara;
        }
        else {
            Player i = find(chara);
            if (i.getMati() == false) {
                status = "Masih hidup";
            }
            else {
                status = "Sudah meninggal dunia dengan damai";
            }
            return i.getTipe() + " " + i.getName() + 
            "\nHP: " + i.getHp() + 
            "\n" + status +
            "\n" + ((i.getTermakan().size() == 0) ? "Belum memakan siapa siapa" : "Memakan " + diet(i.getName()));
        }
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        if (player.size() == 0){
            return "ga ada siapa-siapa";
        }
        else {
            String semua = "";
            for (int i = 0; i < player.size(); i++) {
                semua += status(player.get(i).getName()) + "\n";
            }
            return semua;
        }
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player dummy;
        if (find(chara) == null) {
            return "Tidak ada "+ chara;
        }
        else {
            String dimakan = "";
            dummy = find(chara);
            for (int i = 0; i < dummy.getTermakan().size(); i ++) {
                if (i == dummy.getTermakan().size() - 1) {
                    dimakan += dummy.getTermakan().get(i).getTipe()+ " " + dummy.getTermakan().get(i).getName();
                }
                else {
                    dimakan += dummy.getTermakan().get(i).getTipe()+ " " + dummy.getTermakan().get(i).getName() + ", ";
                }
            }
            return dimakan;
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        if (dataDimakan.size() == 0) {
            return "Belum memakan siapa siapa";
        }
        else {
            String eaten = "";
            for (int i = 0; i < dataDimakan.size(); i++) {
                if (i == dataDimakan.size() -1) {
                    eaten += dataDimakan.get(i).getTipe() + " " + dataDimakan.get(i).getName();
                }
                else {
                    eaten += dataDimakan.get(i).getTipe() + " " + dataDimakan.get(i).getName() + ", ";
                }
            }
            return "Memakan " + eaten;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player satu;
        Player dua;
        if (find(meName) != null) {
            satu = find(meName);
            if (find(enemyName) != null) {
                dua = find(enemyName);
                if (satu.getMati() == false) {
                    return satu.attack(dua);
                }
                else {
                    return satu.getName() + " tidak bisa menyerang " + dua.getName();
                }
            }
            else {
                return "Tidak ada " + enemyName;
            }
        }
        else {
            return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player satu;
        Player dua;
        if (find(meName) != null) {
            satu = find(meName);
            if (find(enemyName) != null) {
                dua = find(enemyName);
                if (satu.getMati() == false) {
                    if (satu.getTipe().equals("Magician")) {
                        Magician penyihir = (Magician)satu;
                        return penyihir.burn(dua);
                    }
                    else {
                        return satu.getName() + " tidak bisa membakar " + dua.getName();
                    }
                }
                else {
                    return satu.getName() + " tidak bisa membakar " + dua.getName();
                }
            }
            else {
                return "Tidak ada " + enemyName;
            }
        }
        else {
            return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player satu;
        Player dua;
        if (find(meName) != null) {
            satu = find(meName);
            if (find(enemyName) != null) {
                dua = find(enemyName);
                if (dua.getMati() == false) {
                    if (satu.canEat(dua) == true) {
                        satu.setDarah(15);
                        satu.addTermakan(dua);
                        dataDimakan.add(dua);
                        player.remove(dua);
                        return satu.getName() + " memakan " + enemyName + 
                        "\nNyawa " + satu.getName() + " kini " + satu.getHp();
                    }
                    else {
                        return satu.getName() + " tidak bisa memakan " + satu.getName();
                    }
                }
                else {
                    return satu.getName() + " tidak bisa mamakan " + satu.getName();
                }
            }
            else {
                return "Tidak ada " + enemyName;
            }
        }
        else {
            return "Tidak ada "+ meName;
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player dummy;
        if (find(meName) == null) {
            return "Tidak ada player " + meName;
        }
        else {
            dummy = find(meName);
            if (dummy.getMati()== false) {
                if (dummy.getTipe().equals("Monster")) {
                    Monster x = (Monster)dummy;
                    return x.roar();
                }
                else {
                    return meName + " Tidak bisa berteriak";
                }
            }
            else {
                return meName + " sudah mati, tidak bisa berteriak";
            }
        }
    }
}