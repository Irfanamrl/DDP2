public class Manusia {
    private String nama;
    private int umur;
    private int uang = 50000;
    private float kebahagiaan = 50;
    private Manusia terakhir;
    private boolean hidup = true;
    
    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        terakhir = this;
    }
    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        terakhir = this;
    }

    // G e t t e r  & S e t t e r
    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setHidup(boolean hidup) {
        this.hidup = hidup;
    }
    public void setUmur(int umur) {
        this.umur = umur;
    }
    public void setUang(int uang) {
        this.uang = uang;
    }
    public void setKebahagiaan(float kebahagiaan) {
        if (kebahagiaan > 100) {
            this.kebahagiaan = 100;
        }
        else if (kebahagiaan < 0) {
            this.kebahagiaan = 0;
        }
        else {
            this.kebahagiaan = kebahagiaan;
        }
    }
    public String getNama() {
        return nama;
    }
    public int getUmur() {
        return umur;
    }
    public int getUang() {
        return uang;
    }
    public float getKebahagiaan() {
        return kebahagiaan;
    }
    public boolean getHidup() {
        return hidup;
    }


    // M e t h o d
    public void beriUang(Manusia penerima) {
        if (getHidup() && penerima.getHidup()) {
            int jumlah = 0;
            for (int i = 0; i<penerima.getNama().length(); i++) {
                int ascii = (int) penerima.getNama().charAt(i);
                jumlah += ascii;
            }
            int jumlahUang = jumlah*100;
            if (getUang() >= jumlahUang) {
                penerima.setUang(penerima.getUang() + jumlahUang);
                setUang(getUang()-jumlahUang);
                setKebahagiaan(getKebahagiaan() + (float)jumlahUang/6000);
                penerima.setKebahagiaan(penerima.getKebahagiaan()+ (float) jumlahUang/6000);
                System.out.println(getNama() + " memberi uang sebanyak " + jumlahUang +
                " kepada "+ penerima.getNama() + ", mereka berdua senang :D");
            }
            else {
                System.out.println(getNama() +"ingin memberi uang kepada "+ penerima.getNama() + 
                "namun tidak memiliki cukup uang :'(");
            }
        }
        else{
            System.out.println(getNama() +" telah tiada");
        }
    }
    public void beriUang(Manusia penerima, int jumlah) {
        if (getHidup() && penerima.getHidup()) {
            if (getUang() >= jumlah) {
                penerima.setUang(penerima.getUang() + jumlah);
                setUang(getUang() - jumlah);
                setKebahagiaan(getKebahagiaan() + (float)jumlah/6000);
                penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)jumlah/6000);
                System.out.println(getNama() +" memberi uang sebanyak "+ Integer.toString(jumlah) +" kepada "+ penerima.getNama() +
                ", mereka berdua senang :D");
            }
            else {
                System.out.println(getNama() +" ingin memberi uang kepada "+ penerima.getNama() +
                " namun tidak memiliki cukup uang :'(");
            }
        }
        else{
            System.out.println("Yang tersisa tinggallah kenangan");
        }
    }
    public void bekerja (int durasi, int bebanKerja) {
        if (getHidup()) {
            int pendapatan;
            if (getUmur() < 18) {
                System.out.println(getNama() +" belum boleh bekerja karena masih dibawah umur D:");
            }
            else {
                int bebanKerjaTotal = durasi*bebanKerja;
                if (bebanKerjaTotal <= getKebahagiaan()) {
                    setKebahagiaan(getKebahagiaan() - (float)bebanKerjaTotal);
                    pendapatan = bebanKerjaTotal*10000;
                    System.out.println(getNama() +" bekerja full time, total pendapatan: "+ pendapatan);
                    setUang(getUang() + pendapatan);
                }
                else {
                    int durasibaru = (int)getKebahagiaan()/bebanKerja;
                    bebanKerjaTotal = durasibaru*bebanKerja;
                    pendapatan = bebanKerjaTotal*10000;
                    setKebahagiaan(getKebahagiaan() - (float)bebanKerjaTotal);
                    System.out.println(getNama() +" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan: "
                    + pendapatan);
                    setUang(getUang() + pendapatan);
                }
            }
        }
        else{
            System.out.println(getNama() + " sudah mati bekerja");
        }
    }
    public void rekreasi (String tempat ) {
        if (getHidup()) {
            int biaya = tempat.length()*10000;
            if (getUang() > biaya) {
                setUang(getUang()-biaya);
                setKebahagiaan(getKebahagiaan() + tempat.length());
                System.out.println(getNama()+" berekreasi di "+ tempat+", "+getNama()+" senang :)");
            }
            else {
                System.out.println(getNama()+" tidak mempunyai cukup uang untuk berekreasi di "+tempat);
            }
        }
        else{
            System.out.println(getNama() + " sudah berekreasi ke tempat yang lebih baik");
        }
    }
    public void sakit(String penyakit) {
        
        if (getHidup()) {
            setKebahagiaan(getKebahagiaan()-penyakit.length());
            System.out.println(getNama()+" terkena penyakit "+penyakit+ " :o");
        }
        else{
            System.out.println("Orang yang sudah mati tidak bisa sakit");
        }
    }
    public void meninggal(){
        if (getHidup()){
            if (terakhir != this) {
                setHidup(false);
                System.out.println(getNama() +"meninggal dengan tenang, kebahagiaan : "+ getKebahagiaan());
                terakhir.setUang(terakhir.getUang() + getUang());
                setUang(getUang()-getUang());
                System.out.println("Semua harta "+getNama()+" disumbangkan untuk"+ terakhir.getNama());
            }
            else {
                setHidup(false);
                setUang(0);
                System.out.println(getNama()+" meninggal dengan tenang, kebahagiaan : "+getKebahagiaan());
				System.out.println("Semua harta "+getNama()+" hangus");
            }
        }
        else {
            System.out.println(getNama()+" telah tiada");
        }
    }
    public String toString() {
        if (getHidup()) {
            return ("Nama\t\t: "+getNama()+"\nUmur\t\t: "+getUmur()+"\nUang\t\t: "
            +getUang()+"\nKebahagiaan\t: "+getKebahagiaan());
        }
        else {
            return ("Nama\t\t: Almarhum "+getNama()+"\nUmur\t\t: "+getUmur()+"\nUang\t\t: "+getUang()+
            "\nKebahagiaan\t: "+getKebahagiaan());
        }
    }
}