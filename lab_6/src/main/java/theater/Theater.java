package theater;
import java.util.ArrayList;
import movie.*;
import ticket.*;

public class Theater {
    private ArrayList<Ticket> ticket;
    private String nama;
    private int saldo;
    private Movie[] film;

    public Theater(String nama, int saldo, ArrayList<Ticket> ticket, Movie[] film) {
        this.nama = nama;
        this.saldo = saldo;
        this.ticket = ticket;
        this.film = film;
    }

    public String getNama() {
        return nama;
    }

    public int getSaldo() {
        return saldo;
    }

    public ArrayList<Ticket> getTicket() {
        return ticket;
    }

    public Movie[] getFilm() {
        return film;
    }



    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public void setTicket(ArrayList<Ticket> ticket) {
        this.ticket = ticket;
    }

    public void getFilm(Movie[] film) {
        this.film = film;
    }

    
    public void printInfo() {
        String output = (
        "------------------------------------------------------------------"+
        "\nBioskop                 : "+ getNama()+
        "\nSaldo Kas               : "+ getSaldo()+
        "\nJumlah tiket tersedia   : "+ getTicket().size()+
        "\nDaftar Film tersedia    : ");

        for (int i = 0; i < film.length ; i++) {
            if(i < film.length-1) {
                output += (film[i].getJudul())+", ";
            }
            else {
                output += (film[i]).getJudul();
            }
        }

        output += ("\n------------------------------------------------------------------");
        System.out.println(output);
    }

    public static void printTotalRevenueEarned(Theater[] theater) {
        int pendapatanTotal = 0;
        for (Theater bioskop: theater) {
            pendapatanTotal += bioskop.getSaldo();
        }

        String hasil = ("Total uang yang dimiliki Koh Mas : Rp. "+pendapatanTotal+
        "\n------------------------------------------------------------------");
        for (int i = 0; i < theater.length ; i++) {
            if (i == theater.length-1) {
                hasil+= 
                ("\nBioskop         : "+ theater[i].getNama()+
                "\nSaldo Kas       : Rp. "+ theater[i].getSaldo()+
                "\n\n------------------------------------------------------------------");
            }
            else {
             hasil += ("\nBioskop         : "+theater[i].getNama() +
             "\nSaldo Kas       : Rp. "+ theater[i].getSaldo()) + "\n";
            }
        }
        System.out.println(hasil);
    }

    public void filterMovieByGenre(String genre) {
        String output = (
        "------------------------------------------------------------------"+
        "\nDaftar Film tersedia    : ");

        for (Movie film : this.getFilm()) {
            if(film.getGenre().toLowerCase().contains(genre.toLowerCase())) {
                output += film.getJudul() + ", ";
            }
        }

        output += ("\n------------------------------------------------------------------");
        System.out.println(output);
    }
}