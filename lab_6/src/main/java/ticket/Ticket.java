package ticket;
import movie.*;
public class Ticket {
    private String judul;
    private String hari;
    private boolean jenis;
    private Movie film;

    public Ticket(Movie film, String hari, boolean jenis) {
        this.film = film;
        this.hari = hari;
        this.jenis = jenis;
    }



    public Movie getFilm() {
        return film;
    }

    public String getHari() {
        return hari;
    }

    public boolean getJenis() {
        return jenis;
    }



    public void setFilm(Movie film) {
        this.film = film;
    }


    public void setHari(String hari) {
        this.hari = hari;
    }

    public void setJenis(boolean jenis) {
        this.jenis = jenis;
    }


    public int getHarga() {
        int harga = 60000;
        if (getHari().equals("Sabtu") || getHari().equals("Minggu")) {
            harga += 40000;
        }
        if (getJenis()) {
            harga = (120 * harga) / 100;
        }
        else{
            harga = harga;
        }
        return harga;
    }



    public String tiketInfo() {
        String output =
        ("------------------------------------------------------------------"+
        "\nFilm            : "+ getFilm()+
        "\nJadwal Tayang   : "+ getHari() +
        "\nJenis           : "+ getJenis() +
        "\n------------------------------------------------------------------");
        return output;
    }
}