package customer;
import theater.*;
import movie.*;
import ticket.*;
import java.util.ArrayList;

public class Customer {
    private String nama;
    private int umur;
    private String kelamin;

    public Customer(String nama, String kelamin, int umur) {
        this.nama = nama;
        this.kelamin = kelamin;
        this.umur = umur;
    }

    public String getNama() {
        return nama;
    }

    public int getUmur(){
        return umur;
    }

    public String getKelamin() {
        return kelamin;
    }


    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setKelamin(String kelamin) {
        this.kelamin= kelamin;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }


    public Ticket orderTicket(Theater theater, String judul, String hari, String jenis) {
        boolean is3D = false;
        if(jenis == "3 Dimensi"){
            is3D = true;
        }else{
            is3D = false;
        }

        for (Ticket ticket: theater.getTicket()) {
            Movie film = ticket.getFilm();
            String rating = film.getRating();

            if (film.getJudul().equals(judul) && ticket.getHari().equals(hari) && ticket.getJenis() == (is3D)) {
                if (rating.equals("Umum")) {
                    System.out.println(nama + " telah membeli tiket "+ judul + " jenis " + jenis +" di " +theater.getNama() +" pada hari " +hari+ " seharga Rp. " + ticket.getHarga());
                    theater.setSaldo(theater.getSaldo() + ticket.getHarga());
                    return ticket;
                }

                if (rating.equals("Remaja")) {
                    if(umur >= 13) {
                        System.out.println(nama + " telah membeli tiket "+ judul + " jenis " + jenis +" di " +theater.getNama() +" pada hari " +hari+" seharga Rp. " + ticket.getHarga());
                        theater.setSaldo(theater.getSaldo() + ticket.getHarga());
                        return ticket;
                    }

                    else {
                        System.out.println(nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + film.getRating());
                        return null;
                    }
                }

                if (rating.equals("Dewasa")) {
                    if (umur >= 17) {
                        System.out.println(nama + " telah membeli tiket "+ judul + " jenis " + jenis +" di " +theater.getNama() +" pada hari " +hari+" seharga Rp. " + ticket.getHarga());
                        theater.setSaldo(theater.getSaldo() + ticket.getHarga());
                        return ticket;
                    }

                    else {
                        System.out.println(nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + film.getRating());
                        return null;
                    }
                }
            }
        }
        System.out.println("Tiket untuk film "+judul+" jenis "+jenis+" dengan jadwal "+hari+" tidak tersedia di "+theater.getNama());
        return null;
    }



    public void findMovie(Theater theater, String judul) {
        Movie[] movie = theater.getFilm();
        int panjang = movie.length;
        for(int i=0; i<panjang;i++){
            if(judul.equals(movie[i].getJudul())){
                System.out.print(movie[i]);
                return;
            }
        }
        System.out.println("Film "+judul+" yang dicari "+nama+" tidak ada di bioskop "+theater.getNama());
        return;

    }
}