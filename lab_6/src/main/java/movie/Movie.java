package movie;
public class Movie{
    //judul genre durasi rating jenis
    private String judul;
    private String genre;
    private int durasi;
    private String rating;
    private String jenis;
    
    public Movie(String judul, String rating, int durasi,String genre, String jenis){
        this.judul = judul;
        this.genre = genre;
        this.durasi = durasi;
        this.rating = rating;
        this.jenis = jenis;
    }

    public String getJudul() {
        return judul;
    }

    public String getGenre() {
        return genre;
    }

    public int getDurasi() {
        return durasi;
    }

    public String getRating() {
        return rating;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJudul(String judul){
        this.judul = judul;
    }

    public void setGenre(String genre){
        this.genre = genre;
    }

    public void setDurasi(int durasi){
        this.durasi = durasi;
    }

    public void getRating(String rating) {
        this.rating = rating;
    }

    public void setJenis(String jenis){
        this.jenis = jenis;
    }

    public String toString(){
        String output = ("------------------------------------------------------------------"+
        "\nJudul   : "+ getJudul() +
        "\nGenre   : "+ getGenre() +
        "\nDurasi  : "+ getDurasi() +" menit" +
        "\nRating  : "+ getRating() +
        "\nJenis   : Film "+ getJenis() +
        "\n------------------------------------------------------------------\n");
        return output;
    }

}